package com.company4;

public class Main {

    public static void main(String[] args) {

            // Создаем парикмахерскую с парикмахером
            BarberShop barberShopSim = new BarberShop();

            Thread barberThread = new Thread(barberShopSim.getBarber());
            barberThread.start();

            while(true) {
                Thread customerThread = new Thread(new Customer(barberShopSim, "Посетитель "));
                customerThread.start();

                try {
                    Thread.sleep( (int)(Math.random()*10000) );
                }
                catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }